import React from 'react';
import './App.css';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';


const api_url = 'https://api.github.com/users';

class App extends React.Component {

  state = { user:{}, active:false };

  handleToggle = () => {
    console.log("dentro do handleToggle")
    fetch(`${api_url}/danderson`)
      .then(res => res.json())
      .then(data => this.setState({user:data, active: true}))        
  }; 

  renderUser = () => {
    console.log("dentro do renderUser")
      return(  
        <div>
              <img src={this.state.user.avatar_url} alt={"avatarusuario"} width={350} />
              <Typography variant="h3" color="primary">
              {this.state.user.name}
              </Typography> 
              <Typography variant="h6">
              {this.state.user.location}
              <br/>
              {this.state.user.blog}
              </Typography>              
             
        </div>          
      )    
  };

  setActive = () => {
    console.log("dentro do setActive")
    this.setState({active: false})
  }
  
  render() {
    console.log("dentro do render")
    if(this.state.active === false) {
      return (
        <div className="App">
          <Button variant="contained" color="primary" onClick={() => this.handleToggle()}>
            Toggle User
          </Button>
        </div>
      )
    }  
    return (
      <div className="App">
        <Button variant="contained" color="primary" onClick={() => this.setActive()}>
          Toggle User
        </Button>
        {this.renderUser()}
      </div>
    )    
  }
}

export default App;
